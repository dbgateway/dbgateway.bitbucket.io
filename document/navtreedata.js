/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE=[["DBGateway","index.html",[["Data Structures","annotated.html",[["Data Structures","annotated.html","annotated_dup"],["Data Fields","functions.html",[["All","functions.html","functions_dup"],["Functions","functions_func.html","functions_func"],["Variables","functions_vars.html","functions_vars"]]]]],["Files","files.html",[["File List","files.html","files_dup"],["Globals","globals.html",[["All","globals.html",null],["Macros","globals_defs.html",null]]]]]]]],NAVTREEINDEX=["_d_b___s_t___d_t___e_l_e_m_8h.html","struct_c_r_c___t___m_a_t_d___r_b_a_r.html#a11192d9e9104f7f6bb0e8debe3dfa6c8","struct_t___e_l_e_m___d__590.html#ac77d8a1c16bd78bd69948b04205cf1c4","struct_t___m_a_t_d___a_l_l__422.html#a6b2ef21d58b1467df16e645301999750","struct_t___m_a_t_d___d__580.html#ad0c5afeb1b96e0dfd40d80d5d82a132d","struct_t___m_a_t_d___d___c_h__702.html#a0b8e1482077b2c39820fe6ff343da3a1","struct_t___m_a_t_l___a_l_l.html#adb458ab6c4b1f00711a944adc4d14201","struct_t___m_a_t_l___d___c_h__550.html#a2165811b8c7d8b08fe557077134a621f","struct_t___m_a_t_l___d_e_s_i_g_n___k_s_c_e05.html#a397c445884320ba8baac00335350b8db","struct_t___r_p_s_c___m_b_a_r.html#ac64cf3bc7505d367ab1489d5de00b23d","struct_t___s_e_c_p___d.html#a8d02a9a932de7edad73aeaf355b415c0","struct_t___s_e_c_t___c_o_m_p_o___s_t_l_g___t_u_b.html#a923336a6d86e0bb1321bed6da654f96a","struct_t___s_e_c_t___s_e_c_t_i_o_n___d.html#aeaf4aa6480212807d44900e240269a29","struct_t___s_e_c_t___s_i_z_e___p_s_c___m_i_d.html#a5e4afa892b7ed26b8ee19a3a45589430","struct_t___s_e_c_t___s_o_d___s_t_i_f_f_n_e_r___i___s_t_i_f_f.html#ac99b7bfa8b89101da115dd9dcc674db3","struct_t___s_e_c_t___s_t_i_f_f_n_e_s_s.html#a9dc2f328dc1d16ed04265260e7b375e4","struct_t___s_t_i_f_f___s_h_a_p_e___d.html#a82acc50d9e3772177211536e3119a13a"],SYNCONMSG="click to disable panel synchronisation",SYNCOFFMSG="click to enable panel synchronisation";
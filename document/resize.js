/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
 */
function initResizable(){function i(i){var e=u+"_"+i+"=";if(document.cookie){var t=document.cookie.indexOf(e);if(-1!=t){var n=t+e.length,o=document.cookie.indexOf(";",n);-1==o&&(o=document.cookie.length);return document.cookie.substring(n,o)}}return 0}function e(i,e,t){if(e!=undefined){if(null==t){var n=new Date;n.setTime(n.getTime()+31536e7),t=n.toGMTString()}document.cookie=u+"_"+i+"="+e+"; expires="+t+"; path=/"}}function t(){var i=($(window).width(),$(d).outerWidth());r.css({marginLeft:parseInt(i)+"px"}),e("width",i-w,null)}function n(i){$(window).width();r.css({marginLeft:parseInt(i)+w+"px"}),d.css({width:i+"px"})}function o(){var i=c.outerHeight(),e=footer.outerHeight(),t=$(window).height()-i-e;r.css({height:t+"px"}),s.css({height:t+"px"}),d.css({height:t+"px"});var n=$(window).width();n!=l&&(n<f&&l>=f?h||a():n>f&&l<f&&h&&a(),l=n),location.hash.slice(1)&&(document.getElementById(location.hash.slice(1))||document.body).scrollIntoView()}function a(){if(d.width()>0)n(0),h=!0;else{var e=i("width");n(e>200&&e<$(window).width()?e:200),h=!1}}var d,s,r,c,h,u="doxygen",l=0,w=6,f=768;c=$("#top"),d=$("#side-nav"),r=$("#doc-content"),s=$("#nav-tree"),footer=$("#nav-path"),$(".side-nav-resizable").resizable({resize:function(){t()}}),$(d).resizable({minWidth:0}),$(window).resize(function(){o()}),navigator.userAgent.toLowerCase().match(/(iphone|ipod|ipad|android)/)&&($(d).css({paddingRight:"20px"}),$(".ui-resizable-e").css({width:"20px"}),$("#nav-sync").css({right:"34px"}),w=20);var g=i("width");g?n(g):t(),o();var p=location.href,v=p.indexOf("#");v>=0&&(window.location.hash=p.substr(v));var m=function(i){i.preventDefault()};$("#splitbar").bind("dragstart",m).bind("selectstart",m),$(".ui-resizable-handle").dblclick(a),$(window).on("load",o)}